package com.sda.constructorExample;

// IS-A relationship
public class Child extends Parent {

    public Child(){
        super(9999);
    }
}
