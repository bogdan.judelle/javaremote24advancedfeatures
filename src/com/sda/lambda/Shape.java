package com.sda.lambda;

public interface Shape {

    double getArea(double side);

}
