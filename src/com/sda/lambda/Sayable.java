package com.sda.lambda;

public interface Sayable {
    void say();
}
