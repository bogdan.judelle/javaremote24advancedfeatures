package com.sda.lambda;

public class Square implements Shape {

    @Override
    public double getArea(double side) {
        return side * side + 23;
    }
}
